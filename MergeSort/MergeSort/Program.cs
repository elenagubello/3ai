﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeSort
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] v = { 5, 7, 9, 1, 8 };
            int[] v = { 8, 7, 6, 5, 4, 3, 2, 1 };
            Console.WriteLine("[{0}]", string.Join(", ", v));
            Sort(v, v.Length);
            Console.WriteLine("Siamo alla fine");
            Console.WriteLine("[{0}]", string.Join(", ", v));
            Console.ReadKey();
        }

        static void Sort(int[] v, int vSize)
        {
            // caso base
            if (vSize < 2)
                return;
            // dividiamo l'array a metà
            int mid = vSize / 2;
            int[] left = new int[mid];
            int[] right = new int[vSize - mid];
            Array.Copy(v, 0, left, 0, mid);
            Array.Copy(v, mid, right, 0, vSize - mid);
            Sort(left, mid);
            Sort(right, vSize - mid);
            Merge(v, left, right);
            Console.WriteLine("[{0}]", string.Join(", ", v));
        }
        static void Merge(int[] v, int[] v1, int[] v2)
        {
            int i = 0, i1 = 0, i2 = 0;
            while (i1 < v1.Length && i2 < v2.Length)
            {
                if (v1[i1] < v2[i2])
                {
                    v[i++] = v1[i1++];
                }
                else
                {
                    v[i++] = v2[i2++];
                }
            }
            while (i1 < v1.Length)
            {
                v[i++] = v1[i1++];
            }
            while (i2 < v2.Length)
            {
                v[i++] = v2[i2++];
            }
        }
    }
}
