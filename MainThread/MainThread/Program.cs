﻿using System;
using System.Threading;

namespace MainThread
{
    class MainThreadProgram
    {
        static void Main(string[] args)
        {
            Thread th = Thread.CurrentThread;
            th.Name = "MainThread";
            Console.WriteLine("Questo è {0}", th.Name);
            Console.ReadKey();
        }
    }
}