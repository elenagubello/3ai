﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Powers
{
    class Powers
    {
        static void Main(string[] args)
        {
            // stampare una tabella con i valori delle potenze x^y,
            // per ogni valore di x tra 1 e 4 e per ogni valore di y
            // tra 1 e 5

            // prima riga
            for(int x = 1; x <= 4; x++)
            {
                for(int y = 1; y <= 5; y++)
                {
                    int p = (int) Math.Pow(x, y);
                    Console.Write(p + " ");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
