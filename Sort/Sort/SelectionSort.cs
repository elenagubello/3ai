﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelectionSort
{
    class SelectionSort
    {
        static void Main(string[] args)
        {
            int[] v = {11, 9, 17, 5, 12};
            Console.WriteLine("[{0}]", string.Join(", ", v));
            for (int i = 0; i < v.Length; i++)
            {
                // cerco il minimo
                int minPos = i;
                int min = v[minPos];
                for (int j = i + 1; j < v.Length; j++)
                {
                    if (min > v[j])
                    {
                        minPos = j;
                        min = v[j];
                    }
                }
                // abbiamo trovato il minimo
                // adesso dobbiamo metterlo nella prima posizione non ordinata
                int tmp = v[i];
                v[i] = v[minPos];
                v[minPos] = tmp;
                Console.WriteLine("[{0}]", string.Join(", ", v));
            }
            Console.ReadKey();
        }
    }
}
