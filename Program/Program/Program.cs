﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            Presentazione(); // chiamata del metodo
            Elaborazione(); // chiamata del metodo
            Console.WriteLine("\n-- Grazie per aver utilizzato il nostro software --");
            Console.ReadKey();
        }

        // definizione del metodo Presentazione
        static void Presentazione()
        {
            Console.WriteLine("-- Calcolo somma di due valori interi --\n\n");
        }

        // definizione del metodo Elaborazione
        static void Elaborazione()
        {
            int a, b, s;
            string tmp;
            Console.Write("Inserisci il primo valore:\t");
            tmp = Console.ReadLine();
            a = Convert.ToInt32(tmp);
            Console.Write("Inserisci il secondo valore:\t");
            tmp = Console.ReadLine();
            b = Convert.ToInt32(tmp);
            s = a + b;
            Console.WriteLine("\nSomma --> " + s);
        }
    }
}
