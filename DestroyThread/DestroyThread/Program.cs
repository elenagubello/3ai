﻿using System;
using System.Threading;

namespace DestroyThread
{
    class Program
    {
        public static void CallToChildThread()
        {
            try
            {
                Console.WriteLine("Child thread");
                for(int i = 0; i <= 10; i++)
                {
                    Thread.Sleep(500);
                    Console.WriteLine(i);
                }
                Console.WriteLine("Child thread finito");
            }
            catch(ThreadAbortException e)
            {
                Console.WriteLine("Thread Abort Exception");
            }
            finally
            {
                Console.WriteLine("Non ho catturato l'eccezione");
            }
        }
        static void Main(string[] args)
        {
            ThreadStart childref = new ThreadStart(CallToChildThread);
            Console.WriteLine("Sono nel main, ho creato il child thread");
            Thread childThread = new Thread(childref);
            childThread.Start();

            // fermiamo il thread principale
            Thread.Sleep(2000);

            // chiudiamo il child thread
            Console.WriteLine("Sono nel main, chiudo il child thread");
            childThread.Abort();
            Console.ReadKey();
        }
    }
}
