﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortF
{
    class SelectionSortF
    {
        static void Main(string[] args)
        {
            int[] v = { 11, 9, 17, 5, 12 };
            Console.WriteLine("[{0}]", string.Join(", ", v));
            for (int i = 0; i < v.Length; i++)
            {
                // cerco il minimo
                // usiamo una funzione
                int minPos = FindMin(v, i, v.Length - 1);
                // abbiamo trovato il minimo
                // adesso dobbiamo metterlo nella prima posizione non ordinata
                // usiamo una funzione
                if (minPos != i)
                {
                    Swap(v, i, minPos);
                    Console.WriteLine("[{0}]", string.Join(", ", v));
                }
            }
            Console.ReadKey();
        }

        private static void Swap(int[] array, int i, int j)
        {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }

        private static int FindMin(int[] array, int from, int to)
        {
            int pos = from;
            int min = array[pos];
            for (int i = from + 1; i <= to; i++)
            {
                if (min > array[i])
                {
                    pos = i;
                    min = array[i];
                }
            }
            return pos;
        }
    }
}
