#include <iostream>      //specificare se l'asse della parabola � parallelo all'asse x o all'asse y, poi calcolare fuoco, vertice, direttrice
#include <ctime>
#include <stdlib.h>

using namespace std;
float XFV (float, float);
float YF (float, float, float);
float YV (float, float, float);
float D (float, float, float);
void Print1 (float, float, float, float);
void Print2 (float, float, float, float);

int main ()
{
    float as, a, b, c, xfv, yf, yv, d;
    cout << "La parabola e' del tipo:\n(1)y = ax^2 + bx + c\no\n(2)x = ay^2 + by + c\n";
    cin >> as;
    if (as!=1 && as!=2)
    {
        cout << "Codice non riconosciuto\nInserisci nuovamente il codice corrispondente al tipo di parabola\n";
        cin >> as;
    }
    cout << "Inserisci i coefficienti a, b, c della parabola data\n";
    cin >> a >> b >> c;
    if (a==0)
    {
        cout << "Questa parabola non esiste\nInserisci nuovamente a\n";
        cin >> a;
    }
    if (as==1)
    {
        cout << "y = " << a << "x^2 + " << b << "x + " << c << endl;
    }
    else
    {
        cout << "x = " << a << "y^2 + " << b << "y + " << c << endl;
    }
    xfv = XFV (a, b);
    yf = YF (a, b, c);
    yv = YV (a, b, c);
    d = D (a, b, c);
    if (as==1) Print1 (xfv, yf, yv, d);
    else Print2 (xfv, yf, yv, d);
    
    system ("PAUSE");
    return 0;
}
float XFV (float a, float b)
{
    float x;
    x = -b/(2*a);
    return x;
}
float YF (float a, float b, float c)
{
    float y, d;
    d = (b*b)-(4*a*c);
    y = (1-d)/(4*a);
    return y;
}
float YV (float a, float b, float c)
{
    float y, d;
    d = (b*b)-(4*a*c);
    y = -d/(4*a);
    return y;
}
float D (float a, float b, float c)
{
    float d, r;
    d = (b*b)-(4*a*c);
    r = (-1-d)/(4*a);
    return r;
}
void Print1 (float xfv, float yf, float yv, float d)
{
     cout << "V( " << xfv << "; " << yv << ")\n";
     cout << "F( " << xfv << "; " << yf << ")\n";
     cout << "d: y = " << d << endl;
}
void Print2 (float xfv, float yf, float yv, float d)
{
     cout << "V( " << yv << "; " << xfv << ")\n";
     cout << "F( " << yf << "; " << xfv << ")\n";
     cout << "d: x = " << d << endl;
}
