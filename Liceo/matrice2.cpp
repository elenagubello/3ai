#include <iostream>
#include <stdlib.h>
#include <ctime>

using namespace std;

const int N=3, M=5;

void Azzera (int [][M]);
void Carica (int [][M]);
void Stampa (int [][M]);
int Somma (int [][M]);

int main ()
{
	int X [N][M];
	Azzera (X);
	Carica (X);
	Stampa (X);
	int s;
	s = Somma (X);
	cout << "Somma: " << s << endl;
	
	system ("PAUSE");
	return 0;
}
void Azzera (int Y[][M])
{
	for (int i=0; i<N; i++)
	{
		for (int j=0; j<M; j++)
		{
			Y[i][j]=0;
		}
	}
}
void Carica (int Y[][M])
{
	srand (time (0));
	for (int i=0; i<N; i++)
	{
		for (int j=0; j<M; j++)
		{
			Y[i][j]= rand ()%50;
		}
	}
}
void Stampa (int Y[][M])
{
	for (int i=0; i<N; i++)
	{
		for (int j=0; j<M; j++)
		{
			cout << Y[i][j] << "\t";
		}
	}
	cout << endl;
}
int Somma (int Y [][M])
{
	int c=0;
	for (int i=0; i<N; i++)
	{
		for (int j=0; j<M; j++)
		{
			c += Y[i][j];
		}
	}
	return c;
}
