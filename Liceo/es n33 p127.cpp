#include <iostream>
#include <ctime>
#include <stdlib.h>

using namespace std;

int verticex (int, int, int);
int verticey (int, int, int);
int fuocoy (int, int, int);

int main ()
{
    int a, b, c, vx, vy, fy;
    cout << "Inserisci i coeffincenti a, b e c dell'equazione di una parabola del tipo y=ax^2+bx+c\n";
    cin >> a >> b >> c;
    vx = verticex (a, b, c);
    vy = verticey (a, b, c);
    fy = fuocoy (a, b, c);
    cout << "V(" << vx <<"; " << vy << ")\n";
    cout << "F(" << vx <<"; " << fy << ")\n";
    
    system ("PAUSE");
    return 0;
}
int verticex (int a, int b, int c)
{
    int v;
    v = -b/(2*a);
    return v;
}
int verticey (int a, int b, int c)
{
    int v;
    v = -((b*b)-(4*a*c))/(4*a);
    return v;
}
int fuocoy (int a, int b, int c)
{
    int f;
    f = (1-((b*b)-(4*a*c)))/4*a;
    return f;
}
