#include <iostream>
#include <stdlib.h>

using namespace std;

void Hanoi(int, char, char, char);
int contamosse = 0;   // contatore delle mosse, variabile globale

int main()
{
    system("CLS");
    system("COLOR f0");
    int n_dischi;
    char piolo_iniziale, piolo_finale, piolo_temporaneo;
    cout << "Risolvo il problema della Torre di Hanoi.\nImmetti il numero di dischi:\n";
    cin >> n_dischi;                                                            // l'utente sceglie il numero di dischi        
    cout << "Immetti l'etichetta (un singolo carattere) del piolo iniziale\n";
    cin >> piolo_iniziale;                                                      // l'utente sceglie le etichette che identificano i tre pioli
    cout << "Immetti l'etichetta (un singolo carattere) del piolo finale\n";
    cin >> piolo_finale;
    cout << "Immetti l'etichetta (un singolo carattere) del piolo temporaneo\n";
    cin >> piolo_temporaneo;
    cout << "Soluzione:\n";
    Hanoi(n_dischi, piolo_iniziale, piolo_finale, piolo_temporaneo);            // chiamata di funzione void
    cout << "\nIl numero di mosse effettuate e' stato: " << contamosse;         // visualizza il numero di mosse, saranno 2^n - 1
    cout << "\n\nFINE!!!\n\n";
    system("PAUSE");
    return 0;
}

void Hanoi(int n, char pi, char pf, char pt)
{
    if ( n == 1 )
    {
         contamosse++;
         cout << "Sposta dal piolo " << pi << " al piolo " << pf << endl;  // sposto  l'unico disco
    }
    else
    {
        Hanoi(n - 1, pi, pt, pf);   // sposta n - 1 dischi dal piolo iniziale a quello temporaneo
        contamosse++;
        cout << "Sposta dal piolo " << pi << " al piolo " << pf << endl;      // sposta l'ultimo piolo a destinazione
        Hanoi(n - 1, pt, pf, pi);
    }
}
