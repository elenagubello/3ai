#include <ctime>
#include <stdlib.h>
#include <iostream>
#include <cmath>

using namespace std;

//Date le coordinate dei vertici di un triangolo, calcolo la misura dei lati, il perimetro e l'area quando possibile

class Triangolo
{
	float ax, ay, bx, by, cx, cy;																																							
	public:
		void Immetti ();
		void Stampa ();
		float Distanza_ab ();
		float Distanza_bc ();
		float Distanza_ac ();
		float Perimetro ();
		float Area ();
};
int main ()
{
	Triangolo t;
	float d_ab, d_bc, d_ac, a, p;
	t.Immetti();
	t.Stampa();
	d_ab = t.Distanza_ab ();
	cout << "AB = " << d_ab << endl;
	d_bc = t.Distanza_bc ();
	cout << "BC = " << d_bc << endl;
	d_ac = t.Distanza_ac ();
	cout << "AC = " << d_ac << endl;
	p = t.Perimetro();
	cout << "2p = " << p << endl;
	
	system ("PAUSE");
	return 0;
}
void Triangolo :: Immetti ()
{
	cout << "Inserire le coordinate di A\n";
	cin >> ax >> ay;
	cout << "Inserire le coordinate di B\n";
	cin >> bx >> by;
	cout << "Inserire le coordinate di C\n";
	cin >> cx >> cy;
}
void Triangolo::Stampa()
{
	cout << "A (" << ax << "; " << ay << ")\n";
	cout << "B (" << bx << "; " << by << ")\n";
	cout << "C (" << cx << "; " << cy << ")\n";
}
float Triangolo::Distanza_ab()
{
	float a, b, c;
	a = ax - bx;
	b = ay - by;
	c = sqrt (a*a + b*b);
	return c;
}
float Triangolo::Distanza_bc()
{
	float a, b, c;
	a = bx - cx;
	b = by - cy;
	c = sqrt (a*a + b*b);
	return c;
}
float Triangolo::Distanza_ac()
{
	float a, b, c;
	a = ax - cx;
	b = ay - cy;
	c = sqrt (a*a + b*b);
	return c;
	}
float Triangolo::Perimetro()
{
	float p, a, b, c;
	a = Distanza_ab ();
	b = Distanza_bc ();
	c = Distanza_ac ();
	p = a + b + c;
	return p;
}
