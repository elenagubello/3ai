#include <iostream>
#include <stdlib.h>

using namespace std;                   //calcolare area, lato, perimetro di un parallelogrammo

float Area (float, float);


int main ()
{
    float area, perimetro, base, lato, altezza;
    cout << "Immetti le misure della base e dell'altezza di un parallelogrammo\n";
    cin >> base >> altezza;
    
    area = Area (base, altezza);
    system ("PAUSE");
    return 0;
}

float Area (float base, float altezza)
{
      return base*altezza;
      
