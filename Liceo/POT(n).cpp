#include <iostream>
#include <ctime>
#include <stdlib.h>

using namespace std;

unsigned int POT (unsigned int);

int main ()
{
    unsigned int n, p;
    cout << "Inserisci un numero n\n";
    cin >> n;
    p = POT(n);
    cout << "POT = " << p << endl;
    system ("PAUSE");
    return 0;
}
unsigned int POT(unsigned int n)
{
    if (n == 1) return 2;
    else
    {
        for (int i=2; i<=n; i++)
        {
            return 2*POT(n-1);
        }
    }
}
