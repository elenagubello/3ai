#include <iostream>
#include <ctime>
#include <stdlib.h>

using namespace std;

/* FATTORIALI
	CASO BASE:
	0!=1	1!=1
	n!= n*(n-1)!	
	Il fattoriale di un numero nturale � uguale al prodotto di
	tutti i numeri naturali che lo precedono*/

unsigned int Fattoriale_ricorsivo (unsigned int);
void Stampa (unsigned int, unsigned int);

int main ()
{
	int n, fatt;
	cout << "Inserisci un numero\n";
	cin >> n;
	fatt = Fattoriale_ricorsivo (n);
	Stampa (fatt, n);
	
	system ("PAUSE");
	return 0;
}

unsigned int Fattoriale_ricorsivo (unsigned int x)
{
	unsigned int g, f;
	if (x == 0 || x == 1) return 1;
	else
	{
		g = x - 1;
		f = x*Fattoriale_ricorsivo (g);
		return f;
	}
}
void Stampa (unsigned int f, unsigned int x)
{
	cout << "il fattoriale di " << x << " � " << f << endl;
}
