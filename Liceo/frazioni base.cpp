#include <iostream>
#include <ctime>
#include <stdlib.h>

using namespace std;

struct Frazione
{
	int n;
	int d;
};
void Print (Frazione);

int main ()
{
	Frazione q, q2;
	cout << "Inserisci i numeratori\n";
	cin >> q.n >> q2.n;
	cout << "Inserisci i denominatori\n";
	cin >> q.d >> q2.d;
	while (q.d==0 || q2.d==0)
	{
		cout << "I denominatori devono essere diversi da 0\n";
	}
	Print (q);
	Print (q2);
	
	system ("PAUSE");
	return 0;
}
void Print (Frazione x)
{
	cout << x.n << "/" << x.d << endl;
}
