#include <stdlib.h>
#include <ctime>
#include <iostream>

using namespace std;
const int N=60;
int main ()
{
    srand (time (0));
    int c1, c2, cont_r = 0, cont_sr = 0;
    float p_r, p_sr;
    for (int i = 0; i < N; i++)
    {
        c1 = 1 + rand () % 40; //estraggo l aprima carta
        c2 = 1 + rand () % 40; //estraggo la seconda carta
        if (c1%10 == 1 || c2%10 == 1) // se una delle due � un asso (gli assi sono 1, 11, 21, 31
        {cont_r++;}
        }
    for (int i = 0; i < N; i++)
    {
        c1 = 1 + rand () % 40;
        do 
        {c2 = 1 + rand () % 40;}
        while (c2 == c1);
        if (c1%10 == 1 || c2%10 == 1)
        {cont_sr++;}
        }
    p_r = 100* cont_r / N;
    p_sr = 100 * cont_sr / N;
    cout << "Volte che � uscito almeno un asso con reinserimento: " << cont_r << "\n" << endl;
    cout << "Volte che � uscito almeno un asso senza reinserimento: " << cont_sr << "\n" << endl;
    cout << "La percentuale con reinserimento � " << p_r << "%\n" << endl;
    cout << "La percentuale senza reiserimento � " << p_sr << "%\n" << endl;
    
    system ("PAUSE");
    return 0;
}
