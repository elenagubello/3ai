#include <iostream>
#include <ctime>
#include <stdlib.h>

using namespace std;

int Cirfa (int);
int Cirfb (int);
int Cirfc (int, int, int);
void Stampa (int,int, int);

int main()
{
	int x, y, r, a, b, c;
	cout << "Inserisci le coordinate del centro e la misura del raggio\n";
	cin >> x >> y >> r;
	while (r==0)
	{
		cout << "La circonferenza non esiste\n";
	}
	a = Cirfa (x);
	b = Cirfb (y);
	c = Cirfc (x, y, r);
	Stampa (a, b, c);
	
	system ("PAUSE");
	return 0;
}
int Cirfa (int x)
{
	return -x*2;
}
int Cirfb (int y)
{
	return -y*2;
}
int Cirfc (int x, int y, int r)
{
	int c;
	c= (x*x)+(y*y)-(r*r);
	return c;
}
void Stampa (int a, int b, int c)
{
	cout << "x^2 + y^2 + " << a << "x + " << b << "y + " << c << " = 0\n";
}
