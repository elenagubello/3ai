#include <iostream>
#include <ctime>
#include <stdlib.h>

/* FRAZIONI
	strutture di controllo, operazioni e mcd */

struct Frazione
{
	int num;
	int den;
};

Frazione Somma (Frazione, Frazione);
void Controllo (Frazione);
Frazione Minterm (Frazione&);
Frazione Sottrazione (Frazione, Frazione);
Frazione Moltiplicazione (Frazione, Frazione);
Frazione Divisione (Frazione, Frazione);
int Scelta ();
void Stampa (Frazione);
Frazione Inserisci ();
Frazione Reciproco (Frazione&);

int main ()
{
	Frazione x, y;
	int n;
	x = Inserisci ();
	y = Inserisci ();
	
	Controllo (x);
	Controllo (y);
	
	Stampa (x);
	Stampa (y);
	
	x = Minterm (x);
	y = Minterm (y);
	cout << "La prima frazione ridotta al minimo termine � " << Stampa (x);
	cout << "La seconda frazione ridotta al minimo termine � " << Stampa (y);
	
	n = Scelta ()
	//Condizioni e chiamate
	if (n == 1) Somma (x, y);
	if (n == 2) Sottrazione (x, y);
	if (n == 3) Moltiplicazione (x, y);
	if (n == 4) Divisione (x, y);
	else {}
	while (n != 1 && n != 2 && n != 3 && n != 4)
	{
		cout << "Il codice scelto non ha corrispondenze\n";
		n = Scelta ();
	}
	//FINE Scelta
	
	system ("PAUSE");
	return 0;
} 
Frazione Inserisci ()
{
	Frazione a;
	cout << "Inserisci il numeratore della frazione\n";
	cin >> a.num;
	cout << "Inserisci il denominatore della frazione\n";
	cin >> a.den;
}
void Controllo (Frazione x)
{
	if (x.den == 0) 
	{
		cout << "La frazione non esiste\nInserirne una nuova\n";
		x = Inserisci ();
	}
	if (x.num == 0)
	{
		cout << "La frazione � uguale a 0\n";
	}
	else cout << "La frazione � accettabile\n";
}
int Scelta ();
{
	int x;
	cout << "Sciegliere l'operazione da svolgere\n\n1- Somma\n2- Sottrazione\n3- Moltiplicazione\n4- Divisione\n";
	cin >> x;
	return x;
}

Frazione Somma (Frazione a, Frazione b)
{
	Frazione s;
	if (a.den == b.den)
	{
		s.num == a.num + b.num;
		s.den = a.den;
	}
	else
	{
		s.num = (a.num*b.den)+(b.num*a.den);
		s.den = a.den*b.den;
	}
	s = Minterm (s);
	return s;
}
Frazione Sottrazione (Frazione a, Frazione b)
{
	Frazione s;
	if (a.den == b.den)
	{
		s.num == a.num - b.num;
		s.den = a.den;
	}
	else
	{
		s.num = (a.num*b.den)-(b.num*a.den);
		s.den = a.den*b.den;
	}
	s = Minterm (s);
	return s;
}
Frazione Moltiplicazione (Frazione a, Frazione b)
{
	Frazione s;
	s.num = a.num * b.num;
	s.den = a.den * b.den;
	s = Minterm (s);
	return s;
}
Frazione Divisione (Frazione a, Frazione b)
{
	b = Reciproco (b);
	Frazione s;
	s.num = a.num * b.num;
	s.den = a.den * b.den;
	s = Minterm (s);
	return s;
}
Frazione Reciproco (Frazione& z)
{
	int t = z.num;
	z.num = z.den;
	z.den = t;
	return 0;
}
int Mcd (int x, int y)
{
	int g;
	g = y-x;
	return g;
}
Frazione Minterm (Frazione x)			//------------------------------------------------------------------------------
{
	/*MCD (a, b) = fattori comuni con esponnete pi� piccolo
	  MCD (a, b) = MCD (a-b, b) while (a- b == 0)
	  MCD (12, 18)
	  12 = 2^2 * 3
	  18 = 2 * 3^2
	  MCD = 2 * 3 = 6
	  MCD (12, 18) = MCD (0, 6)
	  MCD (27, 99)
	  27 = 3^3
	  99 = 3^2 * 11
	  Mcd = 9
	  */
	Frazione m;
	int min;
	if (x.num > x.den)
	{
		//MCD (x.num, x.den)
		while (x.num - x.den != 0)
		{
			min = Mcd (x.num - x-den, x.den);
		}
	}
}
void Stampa (Frazione c)
{
	cout << c.num << "/" << c.den << endl;
}
