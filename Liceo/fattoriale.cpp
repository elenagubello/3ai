//Fattoriale ricorsivo
#include <iostream>
#include <ctime>
#include <stdlib.h>

using namespace std;

unsigned int Fattoriale (unsigned int);
void Stampa (unsigned int, unsigned int);
unsigned int Immetti ();

int main ()
{
	unsigned int n, f;
	n = Immetti ();
	f = Fattoriale (n);
	Stampa (n, f);
	
	system ("PAUSE");
	return 0;
}
unsigned int Immetti ()
{
	unsigned int n;
	cout << "Immetti un numero naturale maggiore di 0\n";
	cin >> n;
	while (n<0)
	{
		cout << "Immetti nuovamente\n";
		cin >> n;
	}
	return n;
}
unsigned int Fattoriale (unsigned int x)
{
	if (x==0 || x==1) return 1;
	else
	{
		return x*Fattoriale (x-1);
	}
}
void Stampa (unsigned int n, unsigned int f)
{
	cout << "Il numero immesso � " << n << endl;
	cout << n << "!= " << f << endl;
}
