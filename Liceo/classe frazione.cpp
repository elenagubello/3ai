#include <iostream>
#include <ctime>
#include <stdlib.h>

using namespace std;

class Frazione
{
      int num;
      int den;
   public:
      Frazione ()
      {
               num = 0;
               den = 1;
      }
      Frazione (int n, int d)
      {
               num = n;
               den = d;
      }
      void Stampa ();
      void Potenza (int);               //int esponente dela frazione
      void Reciproco ();
      void Minterm ();
};

int main ()
{
    Frazione x, y (2, 5);
    x.Stampa ();
    y.Stampa ();
    int e = -2;
    y.Potenza (e);
    cout << "La frazione elevata alla " << e << " e' ";
    y.Stampa ();
    y.Minterm ();
    cout << "Ridotta al mcm e' " << y.Stampa () << endl;
    
    system ("PAUSE");
    return 0;
}
void Frazione :: Stampa ()
{
     cout << num << "/" << den << endl;
}
void Frazione :: Reciproco ()
{
     int t = num;
     num = den;
     den = t;
}
void Frazione :: Potenza (int e)
{
     int n = 1, d = 1;
     if (e < 0)
     {
        Reciproco();
        e *= -1;
     }
     while (e > 0)
     {
             n *= num;
             d *= den;
             e = e - 1;
     }
     num = n;
     den = d;
}
void Frazione :: Minterm ()
{
     if (num == den)
     {
             num = 1;
             den = 1;
     }
     if (num % den ==0)
     {
             num = num /den;
             den = 1;
     }
     if (den % num == 0)
     {
             den = den / num;
             num = 1;
     }
     else
     
