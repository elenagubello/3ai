#include <iostream>
#include <ctime>
#include <stdlib.h>

using namespace std;

int TIME (int);
int main ()
{
    int n, t;
    cout << "Inserisci un numero n\n";
    cin >> n;
    t = TIME(n);
    cout << "TIME = " << t << endl;
    
    system ("PAUSE");
    return 0;
}
int TIME (int n)
{
    int t;
    if (n==0) return 0;
    if (n==1) return 1;
    else
    {
        t = (2*TIME(n-2))+5;
        return t;
    }
}
