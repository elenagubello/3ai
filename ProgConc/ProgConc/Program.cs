﻿using System;
using System.Threading;

namespace ProgConc
{
    class PrimaLezione
    {
        static void Main(string[] args)
        {
            Thread t = new Thread(new ThreadStart(Worker));
            t.Start();
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine("Thread principale");
                Thread.Sleep(0);
            }
            t.Join(); // punto di sincronizzazione tra i thread, torna il thread principale
            Console.WriteLine("Tutti i thread hanno finito");
            Console.ReadKey();
        }

        private static void Worker()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Thread #{0}", i);
                Thread.Sleep(0); // il thread aspetta 0 millisecondi = ha finito, libera la CPU
            }
        }
    }
}
