﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geometria
{
    class Program
    {
        static void Main(string[] args)
        {
            Quadrato q1 = new Quadrato();
            double p1 = q1.Perimetro();
            Console.WriteLine("Il perimetro di q1 è " + p1 + " cm");

            Quadrato q2 = new Quadrato(2);
            double p2 = q2.Perimetro();
            Console.WriteLine("Il perimetro di q2 è " + p2 + " cm");

            Console.ReadKey();
        }
    }
}
