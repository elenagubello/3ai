﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geometria
{
    class Quadrato
    {
        double lato;
        public Quadrato()
        {
            this.lato = 0;
        }

        public Quadrato(double l)
        {
            this.lato = l;
        }

        public double Perimetro()
        {
            double p = lato * 4;
            return p;
        }

        public double Area()
        {
            double a = Math.Pow(lato, 2);
            return a;
        }

        public double Diagonale()
        {
            // ???
            return 0;
        }
    }

    class Cerchio
    {

    }

    class Triangolo
    {
        public bool IsRettangolo()
        {
            return false;
        }
    }
}
